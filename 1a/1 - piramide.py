# Opdracht 1 - Piramide
max = int(input("Hoe groot? "))
i = 0
j = max

c = True
d = True
direction = True

# Met 2 for loops
for a in range(2):
    for b in range(max):
        if a == 0:
            i += 1
        else:
            i -= 1
        print(i * "*")


# Met 2 while loops
while c:
    if direction is False:
        d = True

    while d:
        if direction:
            if i == j:
                direction = False
                d = False
                break
            else:
                i += 1
                print(i * "*")

        elif direction is False:
            i -= 1
            if i == 0:
                d = False
                c = False
            else:
                print(i * "*")


# Met 2 for loops (inverted)
for a in range(2):
    if a == 0:
        print(j * "*")

    for b in range(max):
        if a == 0:
            j -= 1
        else:
            j += 1
        print(j * "*")


# Met 2 while loops (inverted)
while c:
    if j == max:
        print(j * "*")
    if direction is False:
        d = True

    while d:
        if direction:
            if j == 0:
                direction = False
                d = False
                break
            else:
                j -= 1
                print(j * "*")

        elif direction is False:
            if j == max:
                d = False
                c = False
            else:
                j += 1
                print(j * "*")
