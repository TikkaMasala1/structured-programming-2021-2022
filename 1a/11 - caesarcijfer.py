import string

alphabet_string = string.ascii_lowercase
alphabet_list = list(alphabet_string)

test_list = []
test_string = "To be or not to be, That is the question"
for a in test_string:
    test_list.append(a.lower())


def string_caesar_cipher(n):
    changed_list = []

    for b in test_list:

        if b == " ":
            changed_list.append(" ")
            continue
        elif b == ",":
            changed_list.append(",")
            continue

        alphabet_index = alphabet_list.index(b)

        if (alphabet_index + n) >= len(alphabet_list):
            new_index = ((alphabet_index + n) - len(alphabet_list))
        else:
            new_index = alphabet_index + n

        changed_list.append(alphabet_list[new_index])

    print("".join(changed_list))


string_caesar_cipher(1)
