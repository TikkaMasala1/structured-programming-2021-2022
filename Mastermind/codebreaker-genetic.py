import random
from itertools import permutations
import numpy as np

# Based of https://en.wikipedia.org/wiki/Genetic_algorithm


def give_num():
    num = random.sample(range(0, 9), 4)
    return tuple(num)


def play_result(secret, guess):
    X = 0
    Y = 0
    for a, val in enumerate(secret):
        for b, val2 in enumerate(guess):
            if a == b and val == val2:
                X = X + 1
            elif val == val2:
                Y = Y + 1
    return X, Y


def choose_one(code_set):
    remain_table = np.zeros(len(code_set))

    for c, val in enumerate(code_set):
        code = [j for j in range(len(code_set))]
        code.remove(c)

        if len(code) > 100:
            S = random.sample(code, 100)
        else:
            S = random.sample(code, len(code))

        remain = 0

        for d in S:
            A, B = play_result(code_set[d], code_set[c])
            for k in S:
                a, b = play_result(code_set[k], code_set[c])
                if (a == A and b == B):
                    remain = remain + 1

        remain_table[c] = remain

    mindex = np.argmin(remain_table)
    return code_set[mindex]


def ini_population():
    population = permutations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 4)
    return list(population)


# How many times we would like to play
play_number = 10000

# Store the total number
total_num = 0

for f in range(play_number):  # Initialize each game
    code = give_num()  # Create a code
    code_set = ini_population()  # Initialize a set of codes set containing possible answers

    # Create random first guess
    guess = tuple(random.sample(range(0, 9), 4))

    # Make values A and B, the guess and the code respectively
    A, B = play_result(code, guess)
    play_count = 1

    while A < 4:
        # Cleaning the code_set until we find the real answer
        play_count = play_count + 1
        code_set = [t for t in code_set if play_result(t, guess) == (A, B)]
        guess = choose_one(code_set)  # Chooses ant element in the code set for next play

        A, B = play_result(code, guess)

    total_num = total_num + play_count

print("Average number of count: ", total_num / play_number)
