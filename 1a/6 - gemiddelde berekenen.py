import random

random_list = []
for a in range(0, 100):
    n = random.randint(1, 100)
    random_list.append(n)

random_list_list = []
for b in range(0, 100):
    temp_list = []
    for c in range(0, 20):
        n = random.randint(1, 100)
        temp_list.append(n)
    random_list_list.append(temp_list)


def list_average(lst):
    total = 0
    average = 0

    for d in lst:
        total += d

    average = total / len(lst)
    return average


def list_list_average(lst_lst):
    average_lst = []

    for e in lst_lst:
        total = 0

        for f in e:
            total += f

        average = total / len(e)
        average_lst.append(average)
    return average_lst


print(list_average(random_list))
print(list_list_average(random_list_list))
