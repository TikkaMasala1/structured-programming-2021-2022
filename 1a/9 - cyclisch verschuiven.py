def cycle_shuffle(ch, n):
    ch_lst = []
    moved_lst = [0, 0, 0, 0, 0, 0, 0]
    for a in ch:
        ch_lst.append(int(a))

    if n > 0:
        num = n
        lst = ch_lst
    else:
        num = abs(n)
        lst = ch_lst[::-1]

    for b in range(len(lst)):
        if lst[b] == 1:
            if b < num:
                m = num - b
                moved_lst[(len(moved_lst)) - m] = 1
            else:
                moved_lst[b - num] = 1

    if n < 0:
        moved_lst = moved_lst[::-1]

    print(moved_lst)


cycle_shuffle("1011000", 3)
