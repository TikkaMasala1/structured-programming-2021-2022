text1 = "legovogel"
text2 = "ligovogel"


def string_palindrome(str):
    str_reversed = ''.join(reversed(str))
    if str == str_reversed:
        print("De gegeven string is een palidroom")
    else:
        print("De gegeven string is geen palidroom")


def string_palindrome_manual(str):
    str_reversed = str[::-1]
    if str == str_reversed:
        print("De gegeven string is een palidroom")
    else:
        print("De gegeven string is geen palidroom")


string_palindrome(text1)
string_palindrome(text2)
string_palindrome_manual(text1)
string_palindrome_manual(text2)
