n = 20


def fibonacci_rec(i):
    if i <= 1:
        return i
    else:
        return fibonacci_rec(i - 1) + fibonacci_rec(i - 2)


for a in range(n):
    print(fibonacci_rec(a))

