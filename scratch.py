import string

alphabet_string = string.ascii_lowercase
alphabet_list = list(alphabet_string)

lst = []
str = "To be or not to be, That is the question"
for a in str:
    lst.append(a.lower())


def string_caesar_cipher(n):
    cypher_list = []

    for b in lst:

        if b == " ":
            cypher_list.append(" ")
            continue
        elif b == ",":
            cypher_list.append(",")
            continue

        alphabet_index = alphabet_list.index(b)

        if (alphabet_index + n) > len(alphabet_list):
            new_index = ((alphabet_index + n) - len(alphabet_list))
        else:
            new_index = alphabet_index + n

        cypher_list.append(alphabet_list[new_index])

    print("".join(cypher_list))


string_caesar_cipher(4)
