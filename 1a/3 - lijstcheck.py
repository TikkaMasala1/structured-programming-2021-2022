# Opdracht 3 - Lijstcheck
import random

random_list_small = []
for i in range(0, 20):
    n = random.randint(1, 10)
    random_list_small.append(n)

random_list_large = []
for i in range(0, 20):
    n = random.randint(1, 100)
    random_list_large.append(n)

random_list_0_1 = []
for i in range(0, 24):
    n = random.randint(0, 1)
    random_list_0_1.append(n)


def list_occurrence(target, lst):
    counter = 0
    for x in lst:
        if target == x:
            counter += 1
    # print(target, "Komt", counter, "keer voor in de lijst")
    return counter


def list_value_range(lst):
    difference = 0
    index = 0
    for x in range(len(lst)):
        if index == (len(lst) - 2):
            break
        a = abs(lst[index] - lst[index + 1])
        if a > difference:
            difference = a
        index += 2
    # print("Het grootste verschil tussen twee op een volgende getallen is:", difference)
    return difference


def list_0_1(lst):
    # Eisen:
    # Er mogen niet meer dan 12 nullen zijn.
    req_0 = False

    # Het aantal enen is groter dan aan het aantal nullen
    req_1 = False

    counter_0 = list_occurrence(0, lst)

    counter_1 = list_occurrence(1, lst)

    if counter_0 <= 12:
        req_0 = True

    if counter_0 < counter_1:
        req_1 = True

    print("Er mogen niet meer dan 12 nullen zijn is", req_0,
          "\nHet aantal enen is groter dan aan het aantal nullen is", req_1)


print(list_occurrence(10, random_list_small))
print(list_value_range(random_list_large))
list_0_1(random_list_0_1)
